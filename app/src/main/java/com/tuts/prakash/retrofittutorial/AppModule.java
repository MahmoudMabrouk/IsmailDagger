package com.tuts.prakash.retrofittutorial;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tuts.prakash.retrofittutorial.dataLayer.impl.DataLayerModule;
import com.tuts.prakash.retrofittutorial.util.Constants;
import com.tuts.prakash.retrofittutorial.util.PreferenceHelper;
import com.tuts.prakash.retrofittutorial.viewModel.impl.ViewModelModule;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


@Module(includes = {DataLayerModule.class, ViewModelModule.class})
public class AppModule {

    private Application app;

    AppModule(Application app) {
        this.app = app;
    }

    @Singleton
    @Provides
    Context providesContext () {
        return app;
    }

    @Singleton
    @Provides
    PreferenceHelper providesPreferenceHelper(Context context) {
        return  new PreferenceHelper(context);
    }
    
    //// TODO: 8/7/18 i do not know the place of them  
    @Singleton
    @Provides
    Gson provideGson(){
        
        return  new GsonBuilder()
                .setLenient()
                .create();
    }

    @Singleton
    @Provides
    OkHttpClient providesOkHttpClint (){
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
        
        return okHttpClient ; 
    }

    //// TODO: 8/7/18  can not deal with generic here
  /*  @Singleton
    @Provides
    Retrofit providesRetrofit(Gson gson , OkHttpClient okHttpClient ,Class clazz){
        Retrofit retrofit = new Retrofit
                .Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(Constants.RETROFIT_BASE_URL)
                .client(okHttpClient)
                .build();
        return  retrofit.create(clazz);
    }*/


//    @Singleton
//    @Provides
//    ImageLoader providesImageLoader(Context context){
//        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
//                .memoryCacheExtraOptions(480, 800)
//                .diskCacheExtraOptions(480, 800, null)
//                .threadPoolSize(3)
//                .threadPriority(Thread.NORM_PRIORITY - 2)
//                .tasksProcessingOrder(QueueProcessingType.FIFO)
//                .denyCacheImageMultipleSizesInMemory()
//                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
//                .memoryCacheSize(2 * 1024 * 1024)
//                .memoryCacheSizePercentage(25)
//                .diskCacheSize(50 * 1024 * 1024)
//                .diskCacheFileCount(100)
//                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator())
//                .imageDownloader(new BaseImageDownloader(context))
//                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
//                .writeDebugLogs()
//                .build();
//        ImageLoader.getInstance().init(config);
//        return ImageLoader.getInstance();
//    }
}
